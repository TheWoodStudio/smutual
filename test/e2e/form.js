const faker = require('faker')

const randomName = faker.name.firstName()
const randomLastName = faker.name.lastName()
const randomEmail = faker.internet.email()
// const randomPhone = faker.phone.phoneNumberFormat()
const randomPhone = Math.floor((Math.random() * 100000000))
const numDNI = Math.floor((Math.random() * 100000000))

module.exports = {
  'Contact Form': function (client) {
    client
      .url('http://localhost:8000')
      .waitForElementVisible('body', 500)

      .verify.elementPresent('#name')
      .verify.elementPresent('#surname')
      .verify.elementPresent('#identity')
      .verify.elementPresent('#email')
      .verify.elementPresent('#phone')

      .setValue('#name', randomName)
      .setValue('#surname', randomLastName)
      .setValue('#identity', numDNI)
      .setValue('#email', randomEmail)
      .setValue('#phone', randomPhone)

      .click('.send-button')

      .pause(1000)

      .assert.containsText('#output', 'El formulario se ha enviado correctamente')

      .saveScreenshot('reports/form-test.png')

      .pause(1000)

      .end()
  }
}
