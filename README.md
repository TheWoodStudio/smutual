# Amoemra site


## Build Setup

``` bash
# run nginx instance in localhost:8000
docker-compose up -d
```

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```


