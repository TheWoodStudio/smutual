import Vue from 'vue'
import Router from 'vue-router'
import Header from '@/components/header/Header'
import Carousel from '@/components/carousel/Carousel'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'App',
      components: {
        Header,
        Carousel
      }
    }
  ]
})
