// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'babel-polyfill'
// import App from './App'
// import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './sass/styles.scss'
import HeaderApp from '@/components/header/Header'
import Carousel from '@/components/carousel/Carousel'
import Information from '@/components/information/Information'
import Benefits from '@/components/benefits/Benefits'
import Extras from '@/components/extras/Extras'
import Experiences from '@/components/experiences/Experiences'
import ContactForm from '@/components/form/Form'
import MapSection from '@/components/map/Map'
import FooterApp from '@/components/footer/Footer'

import { MdField, MdButton } from 'vue-material/dist/components'
import VeeValidate from 'vee-validate'
import Meta from 'vue-meta'
import vueSmoothScroll from 'vue-smooth-scroll'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(MdField)
Vue.use(MdButton)
Vue.use(VeeValidate)
Vue.use(Meta)
Vue.use(vueSmoothScroll)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    HeaderApp,
    Carousel,
    Information,
    Benefits,
    Extras,
    Experiences,
    ContactForm,
    MapSection,
    FooterApp
  }
})
