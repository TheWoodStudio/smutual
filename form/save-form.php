<?php

$firstname = $_POST['name'];
$lastname = $_POST['surname'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$dni = $_POST['identity'];

$responseData = array();

if(empty($_POST["name"])) {
  $responseData['success'] = false;

  header('Content-Type: application/json');
  echo json_encode($responseData);
  die();
}

try {

  $file_open = fopen("contacts.csv", "a");
  $form_data = array(
    'firstname'		=>	$firstname,
    'lastname'		=>	$lastname,
    'dni'	=>	$dni,
    'email'		=>	$email,
    'phone'	=>	$phone
  );
  fputcsv($file_open, $form_data);

  $responseData['success'] = true;
  $responseData['message'] = 'Success';

} catch (\Exception $e) {
  $responseData['success'] = false;
  $responseData['message'] = $e->getMessage();
}

try {
  $arr = array(
    'properties' => array(
        array(
            'property' => 'email',
            'value' => $email
        ),
        array(
            'property' => 'firstname',
            'value' => $firstname
        ),
        array(
            'property' => 'lastname',
            'value' => $lastname
        ),
        array(
            'property' => 'phone',
            'value' => $phone
        ),
        array(
            'property' => 'dni',
            'value' => $dni
        )
      )
  );

  $post_json = json_encode($arr);
  $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $_ENV['_CRM_API_KEY.'];
  $ch = @curl_init();
  @curl_setopt($ch, CURLOPT_POST, true);
  @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
  @curl_setopt($ch, CURLOPT_URL, $endpoint);
  @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = @curl_exec($ch);
  $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
  $curl_errors = curl_error($ch);
  @curl_close($ch);
} catch (\Exception $e) {
  $responseData['success'] = false;
  $responseData['message'] = $e->getMessage();
}

header('Content-Type: application/json');
echo json_encode($responseData);
